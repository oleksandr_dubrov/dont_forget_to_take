package com.dontforgettotake;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dontforgettotake.database.Product;
import com.dontforgettotake.database.RecyclerViewItem;

import java.util.ArrayList;
import java.util.List;


final public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.MyViewHolder> {

    private static final String TAG = "HomeAdapter";
    private List<RecyclerViewItem<Product>> mDataset = new ArrayList<>();
    private List<RecyclerViewItem<Product>> mOriginDataset = null;

    @NonNull
    @Override
    public HomeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_item_check_text, parent, false);
        return new HomeAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeAdapter.MyViewHolder holder, int position) {
        RecyclerViewItem<Product> product = mDataset.get(position);
        holder.checkBox.setChecked(product.getItem().isChecked());
        holder.textView.setText(product.getItem().getName());
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    void setData(@NonNull List<Product> products) {
        mDataset.clear();
        for (Product pr :  products) {
            mDataset.add(new RecyclerViewItem<>(pr.getName(), pr, null, false));
        }
        notifyDataSetChanged();
    }

    /**
     * Get the adapter data in a object like it was set.
     * @return a list of products
     */
    @NonNull
    List<Product> getData() {
        final List<Product> products = new ArrayList<>(mDataset.size());
        for (RecyclerViewItem<Product> rvi : mDataset) {
            products.add(rvi.getItem());
            if (rvi.getItem().isChecked()) {
                Log.i(TAG, rvi.getItem().getName() + " checked.");
            }
        }
        return products;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textView;
        CheckBox checkBox;
        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.tv_item_cbx);
            checkBox = itemView.findViewById(R.id.checkBox);
            View.OnClickListener listener = v -> {
                int posIdx = getAdapterPosition();
                Product cur = mDataset.get(posIdx).getItem();
                Log.i(TAG, cur.getName() + " changes the check mark.");
                cur.setChecked(!cur.isChecked());
                checkBox.setChecked(cur.isChecked());
            };
            // set the listener to both views
            checkBox.setOnClickListener(listener);
            itemView.setOnClickListener(listener);
        }
    }

    void filter(@NonNull String query) {
        final String lowerCaseQuery = query.toLowerCase();
        final List<RecyclerViewItem<Product>> newDataSet = new ArrayList<>();

        if (mOriginDataset == null) mOriginDataset = mDataset;
        for (RecyclerViewItem<Product> rvi : mOriginDataset) {
            final String[] words = rvi.getName().toLowerCase().split(" ");
            boolean found = false;
            for (String word: words) {
                if (word.startsWith(lowerCaseQuery)) {
                    found = true;
                    break;
                }
            }
            if (found) newDataSet.add(rvi);
        }
        mDataset = newDataSet;
        notifyDataSetChanged();
    }

    void restoreOriginData() {
        if (mOriginDataset != null) {
            mDataset = mOriginDataset;
            mOriginDataset = null;
            notifyDataSetChanged();
        }
    }
}
