package com.dontforgettotake;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.dontforgettotake.database.Category;
import com.dontforgettotake.database.CategoryWithProducts;
import com.dontforgettotake.database.Product;
import com.dontforgettotake.database.ProductRepository;
import com.dontforgettotake.database.RemoteDatabase;

import java.io.Serializable;
import java.util.List;


final public class ProductViewModel extends AndroidViewModel implements Serializable {
    private ProductRepository mRepository;
    private LiveData<List<Product>> mAllProducts;
    private LiveData<List<CategoryWithProducts>> mAllCategories;

    ProductViewModel(Application application,
                     @Nullable String url,
                     RemoteDatabase.LastUpdateListener listener) {
        super(application);
        mRepository = new ProductRepository(application, url, listener);
        mAllProducts = mRepository.getAllProducts();
        mAllCategories = mRepository.getAllCategories();
    }

    void fetchRemote() {
        mRepository.fetchRemote();
    }

    List<CategoryWithProducts> getRemoteCategories() {
        return mRepository.getAllRemoteCategories();
    }

    public LiveData<List<Product>> getAllProducts() {
        return mAllProducts;
    }

    public LiveData<List<CategoryWithProducts>> getAllCategories() {
        return mAllCategories;
    }

    public void insertProduct(Product product) {
        mRepository.insert(product);
    }

    void insertCategoriesWithProducts(@NonNull List<CategoryWithProducts> categoryWithProducts) {
        for (CategoryWithProducts dp : categoryWithProducts) {
            insertProducts(dp.products);
            mRepository.insert(dp.category);
        }
    }

    public void insertCategory(Category category) {
        mRepository.insert(category);
    }

    public void delete(Product product) {
        mRepository.delete(product);
    }

    public void delete(Category category) {mRepository.delete(category);}

    void insertProducts(@NonNull List<Product> products) {
        for (Product product : products) {
            mRepository.insert(product);
        }
    }

    void updateProducts(@NonNull List<Product> products) {
        for (Product product : products) {
            mRepository.update(product);
        }
    }

    public void updateCategoriesWithProducts(@NonNull List<CategoryWithProducts> categoryWithProducts) {
        for (CategoryWithProducts dp : categoryWithProducts) {
            updateProducts(dp.products);
            mRepository.update(dp.category);
        }
    }

    void updateCategories(@NonNull List<Category> categories) {
        for (Category category: categories) {
            mRepository.update(category);
        }
    }
}
