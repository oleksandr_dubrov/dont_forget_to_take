package com.dontforgettotake;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.preference.PreferenceManager;

import com.dontforgettotake.database.CategoryWithProducts;
import com.dontforgettotake.database.Product;
import com.dontforgettotake.database.RemoteDatabase;
import com.dontforgettotake.modify.ModifyCategoryActivity;
import com.dontforgettotake.modify.ModifyProductActivity;
import com.dontforgettotake.utils.XmlParser;

import org.xmlpull.v1.XmlPullParserException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Objects;


public abstract class AbstractActivity extends AppCompatActivity
        implements ViewModelProvider.Factory, RemoteDatabase.LastUpdateListener {

    private ProductViewModel mProductViewModel;
    private SharedPreferences sharedPref;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get a new or existing ViewModel from the ViewModelProvider.
        mProductViewModel = new ViewModelProvider(this, this).get(ProductViewModel.class);
        sharedPref = getPreferences(Context.MODE_PRIVATE);
    }

    protected SharedPreferences getSharedPref() {
        return sharedPref;
    }

    /**
     * Configure the view model rithg after created.
     * @param modelClass  the view model class
     * @param <T> the view model type
     * @return  a new instance of the view model
     */
    @Override
    @NonNull
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(ProductViewModel.class)) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            String remote_address = sharedPreferences.getString("remote_address", null);
            ProductViewModel productViewModel = new ProductViewModel(getApplication(), remote_address, this);
            return Objects.requireNonNull(modelClass.cast(productViewModel));
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }

    ProductViewModel getViewModel() {
        return mProductViewModel;
    }

    /**
     * Persist date the activity operates with.
     * Update the timestamp in the base class, do more in successors.
     */
    protected void persist() {
        final long ts = System.currentTimeMillis()/1000;
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong("last_update", ts);
        editor.apply();
    }

    /**
     * Sync the list list with remove server
     */
    protected void sync() {
        mProductViewModel.fetchRemote();
    }

    @Override
    public void onLastUpdate(long lastRemoteUpdate) {}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    // https://stackoverflow.com/questions/36557879/how-to-use-native-android-file-open-dialog
    private void getXmlFileToImport() {
        Intent intent = new Intent()
                .setType("*/*")
                .setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_XML_file)),123);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 123 && resultCode == RESULT_OK) {
            Uri selectedFile = data.getData(); //The uri with the location of the file
            assert selectedFile != null;
            XmlParser parser = new XmlParser();
            ContentResolver cr = getContentResolver();
            try {
                List<CategoryWithProducts> categories = parser.parse(cr.openInputStream(selectedFile));
                int nbrCat = countProductsInCategories(categories);
                String msg = String.format(getString(R.string.import_report),
                                            nbrCat, categories.size());
                showAlertDialogWithOk("Import", msg);
                insertCategory(categories);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }
        }
    }

    private void insertCategory(@NonNull List<CategoryWithProducts> categories) {
        for (CategoryWithProducts category : categories) {
            mProductViewModel.insertCategory(category.getCategory());
            for (Product product : category.getProducts()) {
                product.setCategory(category.getName());
                mProductViewModel.insertProduct(product);
            }
        }
    }

    private int countProductsInCategories(@NonNull List<CategoryWithProducts> categories) {
        int ret = 0;
        for (CategoryWithProducts category : categories) {
            ret += category.getProducts().size();
        }
        return ret;
    }

    private void showAlertDialogWithOk(String title, String msg) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                (dialog, which) -> dialog.dismiss());
        alertDialog.show();
    }

    /**
     * Intent to send a telegram message
     * @param msg a message to send
     */
    void intentMessageTelegram(String msg)
    {
        final String appName = "org.telegram.messenger";
        final boolean isAppInstalled = isAppAvailable(getApplicationContext());
        if (isAppInstalled)
        {
            Intent myIntent = new Intent(Intent.ACTION_SEND);
            myIntent.setType("text/plain");
            myIntent.setPackage(appName);
            myIntent.putExtra(Intent.EXTRA_TEXT, msg);
            startActivity(Intent.createChooser(myIntent, getString(R.string.share_via_telegram)));
        }
        else {
            Toast.makeText(this, "Telegram is not installed", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Indicates whether the specified app ins installed and can used as an intent. This
     * method checks the package manager for installed packages that can
     * respond to an intent with the specified app. If no suitable package is
     * found, this method returns false.
     *
     * @param context The application's environment.
     * @return True if app is installed
     */
    private static boolean isAppAvailable(@NonNull Context context) {
        PackageManager pm = context.getPackageManager();
        try  {
            pm.getPackageInfo("org.telegram.messenger", PackageManager.GET_ACTIVITIES);
            return true;
        }
        catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public boolean onPrepareOptionsMenu(@NonNull Menu menu) {
        if (this instanceof MainActivity) {
            menu.findItem(R.id.action_collapseAll).setVisible(true);
            menu.findItem(R.id.action_share).setVisible(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;

            case R.id.action_share:
                // https://stackoverflow.com/questions/21627167/how-to-send-a-intent-with-telegram
                String msg = getStringFromList();
                if (msg == null || msg.isEmpty()) {
                    showAlertDialogWithOk("Error", getString(R.string.share_error_msg));
                }
                else {
                    intentMessageTelegram(msg);
                }
                return true;

            case R.id.action_about :
                showAlertDialogWithOk("About", "Don't forget to take.");
                return true;

            case R.id.modify :
                return true;

            case R.id.modify_categories:
                Intent intent1 = new Intent(this, ModifyCategoryActivity.class);
                intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent1);
                return true;

            case R.id.modify_products:
                Intent intent2 = new Intent(this, ModifyProductActivity.class);
                intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent2);
                return true;

            case R.id.action_import_list:
                getXmlFileToImport();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    /**
     * Get all names from the list as a new line separated string.
     * @return a string of names
     */
    protected abstract String getStringFromList();

}
