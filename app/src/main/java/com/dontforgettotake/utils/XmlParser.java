package com.dontforgettotake.utils;

import android.util.Xml;

import androidx.annotation.NonNull;

import com.dontforgettotake.database.Category;
import com.dontforgettotake.database.CategoryWithProducts;
import com.dontforgettotake.database.Product;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


final public class XmlParser {

    final private static String PRODUCTS_TAG = "products";
    final private static String CATEGORY_TAG = "department";
    final private static String PRODUCT_TAG = "product";

    private static final String ns = null;

    @NonNull
    public List<CategoryWithProducts> parse(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readFeed(parser);
        } finally {
            in.close();
        }
    }

    @NonNull
    private List<CategoryWithProducts> readFeed(@NonNull XmlPullParser parser) throws XmlPullParserException, IOException {
        List<CategoryWithProducts> categories = new ArrayList<>();

        parser.require(XmlPullParser.START_TAG, ns, PRODUCTS_TAG);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            // Starts by looking for the entry tag
            if (name.equals(CATEGORY_TAG)) {
                categories.add(readCategory(parser));
            } else {
                throw new IOException();
            }
        }
        return categories;
    }

    private CategoryWithProducts readCategory(@NonNull XmlPullParser parser) throws XmlPullParserException, IOException {
        List<Product> products = new ArrayList<>();
        parser.require(XmlPullParser.START_TAG, ns, CATEGORY_TAG);
        String depName = parser.getAttributeValue(null, "name");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("product")) {
                products.add(readProduct(parser));
            } else {
                throw new IOException();
            }
        }
        Category category = new Category(depName);
        return new CategoryWithProducts().setCategory(category).setProducts(products);
    }

    @NonNull
    private Product readProduct(@NonNull XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, PRODUCT_TAG);
        String inlist = parser.getAttributeValue(null, "inlist");
        String prod_name = parser.getAttributeValue(null, "name");
        parser.nextTag();
        parser.require(XmlPullParser.END_TAG, ns, PRODUCT_TAG);
        Product product = new Product(prod_name);
        product.setChecked(inlist.equals("1"));
        return product;
    }

    // Extracts text values.
    private String readText(@NonNull XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }


}
