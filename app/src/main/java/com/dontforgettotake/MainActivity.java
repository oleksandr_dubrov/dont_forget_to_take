package com.dontforgettotake;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.dontforgettotake.database.CategoryWithProducts;
import com.dontforgettotake.database.Product;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.MenuItem;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static androidx.recyclerview.widget.ItemTouchHelper.DOWN;
import static androidx.recyclerview.widget.ItemTouchHelper.RIGHT;
import static androidx.recyclerview.widget.ItemTouchHelper.UP;


// TODO: long press on product should open a config dialog
//      That might be like a merge of modify activities and dialogs to the home activity
//      Long press on Category should collapse or extend it.
// TODO: add help information
// TODO: get red from modify activities (if possible)
public class MainActivity extends AbstractActivity {

    private static final String TAG = "MainActivity";
    private MainAdapter mAdapter;
    private ProductViewModel mProductViewModel;
    private List<CategoryWithProducts> mCategoriesWithProducts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mProductViewModel = getViewModel();

        FloatingActionButton fab = findViewById(R.id.fab_plus);
        fab.setOnClickListener(view -> {
                    Intent intent = new Intent(this, HomeActivity.class);
                    startActivity(intent);
                });

        createRecyclerView();
        setSwipeToRefresh();
        applySettings();
    }

    @Override
    protected void onStop() {
        super.onStop();
        persist();
    }

    private void createRecyclerView() {
        RecyclerView recyclerView = findViewById(R.id.my_recycler_view);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);


        // https://proandroiddev.com/enter-animation-using-recyclerview-and-layoutanimation-part-1-list-75a874a5d213
        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerView.setLayoutAnimation(animation);

        // specify an adapter
        mAdapter = new MainAdapter(this);
        recyclerView.setAdapter(mAdapter);

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(UP|DOWN, RIGHT) {
//            @Override
//            public boolean canDropOver(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder current,
//                                       @NonNull RecyclerView.ViewHolder target){
//                return mAdapter.canBeDragged(current.getAdapterPosition());
//            }
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                if (i == RIGHT) {
                    int pos = viewHolder.getAdapterPosition();
                    mAdapter.uncheckItem(pos);
                    Product p = mAdapter.getProductById(pos);
                    if (p != null) mProductViewModel.updateProducts(Collections.singletonList(p));
                    else {
                        CategoryWithProducts c = mAdapter.getCategory(pos);
                        if (c == null) throw new AssertionError();
                        mProductViewModel.updateCategoriesWithProducts(Collections.singletonList(c));
                    }
                    MainActivity.super.persist();
                }
            }
        }).attachToRecyclerView(recyclerView);

        // Add an observer on the LiveData returned by getAllCategories.
        // The onChanged() method fires when the observed data changes and the activity is
        // in the foreground.
        mProductViewModel.getAllCategories().observe(this, categories -> {
            mCategoriesWithProducts = categories;
            // Update the cached copy of the words in the adapter.
            mAdapter.setData(categories);
        });
    }

    private void setSwipeToRefresh() {
        /*
         * Sets up a SwipeRefreshLayout.OnRefreshListener that is invoked when the user
         * performs a swipe-to-refresh gesture.
         */
        SwipeRefreshLayout swipeRefreshLayout = findViewById(R.id.swipe_layout);
        swipeRefreshLayout.setOnRefreshListener(
                () -> {
                    // This method performs the actual data-refresh operation.
                    // The method calls setRefreshing(false) when it's finished.
                    persist(); // the observed data is changing so it will fire the update
                    sync();
                    swipeRefreshLayout.setRefreshing(false);
                }
        );
    }

    private void applySettings() {
        // FIXME: no data in the adapter yet
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean bCollapse = sharedPreferences.getBoolean("collapse_by_default", true);
        if (!bCollapse) mAdapter.expendAll();
    }

    @Override
    @NonNull
    protected String getStringFromList() {
        StringBuilder res = new StringBuilder();
        for (Product product : getListOfProducts()) {
            res.append(product.getName()).append('\n');
        }
        return res.toString();
    }

    @Override
    protected void persist() {
        // If a product is unchecked then increment it order to make it more popular.
        for (Product product : getListOfProducts()) {
            if (!product.isChecked()) product.incrementOrder();
        }
        mProductViewModel.updateCategoriesWithProducts(mAdapter.getData());
        super.persist();
    }

    @NonNull
    private List<Product> getListOfProducts() {
        List<CategoryWithProducts> categoriesWithProducts = mAdapter.getData();
        List<Product> products = new ArrayList<>(categoriesWithProducts.size());
        for (CategoryWithProducts categoryWithProducts : categoriesWithProducts) {
            products.addAll(categoryWithProducts.getProducts());
        }
        return products;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_collapseAll) {
            mAdapter.collapseAll();
            return true;
        }// If we got here, the user's action was not recognized.
        // Invoke the superclass to handle it.
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLastUpdate(long lastRemoteUpdate) {
        if (lastRemoteUpdate == 0) {
            Log.i(TAG, "Cannot sync with the remote server.");
            return;
        }
        super.onLastUpdate(lastRemoteUpdate);
        long lastLocalUpdate = getSharedPref().getLong("last_update", 0);
        if (lastRemoteUpdate > lastLocalUpdate) {
            List<CategoryWithProducts> remoteCategories = mProductViewModel.getRemoteCategories();
            if(remoteCategories.size() > 0) {
                List<CategoryWithProducts> toUpdate = new ArrayList<>();
                List<CategoryWithProducts> toInsert = new ArrayList<>();
                for (CategoryWithProducts rc : remoteCategories) {
                    boolean flag = false;
                    for (CategoryWithProducts lc : mCategoriesWithProducts) {
                        if (rc.getName().equals(lc.getName())) {
                            toUpdate.add(rc);
                            flag = true;
                            break;
                        }
                    }
                    if (!flag) toInsert.add(rc);
                }
                mProductViewModel.updateCategoriesWithProducts(toUpdate);
                mProductViewModel.insertCategoriesWithProducts(toInsert);
            }
        }
        else {

        }
    }

}
