package com.dontforgettotake.database;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "product_table")
public class Product {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "name")
    public String mName;

    @ColumnInfo(name = "category_name")
    public String mCategoryName;

    @ColumnInfo(name = "checked")
    public boolean mChecked;

    @ColumnInfo(name = "order")
    public int mOrder;

    public Product(@NonNull String name) {
        mName = name;
        mChecked = false;
        mOrder = 0;
    }

    public void setCategory(String category) {
        mCategoryName = category;
    }

    @NonNull
    @Override
    public String toString() {
        return mName;
    }

    public boolean isChecked() {
        return mChecked;
    }

    public void setChecked(boolean checked) {
        mChecked = checked;
    }

    public String getCategoryName() {
        return mCategoryName;
    }

    public void incrementOrder() {
        mOrder++;
    }

    public int getOrder() {
        return mOrder;
    }

    public void setOrder(int order) {
        this.mOrder = order;
    }

    public String getName() {
        return mName;
    }
}
