package com.dontforgettotake.database;

import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public final class RemoteDatabase {
    private static final String AUTHORIZATION = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTU4OTczMDY3N30.BA6evYltrG9g3h3A7pTVCpMxp8foAy5bD-mC-NB5rxpLz1G3jcSTbYq1s2kF-JNnnOZtcYhzpSLGnI9ctobtqA";
    private static final String TAG = "RemoteDatabase";
    private static volatile RemoteDatabase INSTANCE;
    private static final String PATH = "/api/lists/";
    private final String url;

    private long lastUpdate;
    private JSONArray respWithCategories = null;
    private LastUpdateListener listener;

    private RemoteDatabase(String url, LastUpdateListener listener){
        this.url = processUrl(url);
        this.listener = listener;
        lastUpdate = 0;
    }

    static RemoteDatabase getDatabase(String url, LastUpdateListener listener) {
        if (INSTANCE == null) {
            INSTANCE = new RemoteDatabase(url, listener);
        }
        return INSTANCE;
    }

    void fetch() {
        MyTask mt = new MyTask((result) -> {
            if (result == null) {
                // inform the caller that we coudn't get remote data.
                listener.onLastUpdate(0);
                return;
            }
            String url = parseLists(result);
            if (url == null) {
                Log.e(TAG, "Category URL is not found.");
            }
            else {
                MyTask t = new MyTask((new_result) -> {
                    respWithCategories = getArrayOfCategories(new_result);
                    listener.onLastUpdate(lastUpdate);
                });
                t.execute(url);
            }
        });
        mt.execute(url + PATH);
    }

    List<CategoryWithProducts> getCategories() {
        List<CategoryWithProducts> categoriesWithProducts = null;
        try {
            if (respWithCategories != null) {
                categoriesWithProducts = getFromJson(respWithCategories);
            }
        } catch (JSONException e) {
            Log.e(TAG, e.toString());
        }
        return categoriesWithProducts;
    }

    @NonNull
    private String processUrl(@NonNull String url) {
        String http = "http://";
        if (!url.startsWith(http)) url = http + url;
        return url;
    }

    @NonNull
    private static List<CategoryWithProducts> getFromJson(@NonNull JSONArray jsonArray) throws JSONException {
        // unfortunately, bad domain structure force me not to use Gson
        final List<CategoryWithProducts> categoriesWithProducts = new ArrayList<>(jsonArray.length());
        for (int idx = 0; idx < jsonArray.length(); ++idx) {
            final JSONObject jsonCategory = jsonArray.getJSONObject(idx);
            final JSONArray jsonProducts = jsonCategory.getJSONArray("products");
            final Category category = new Category(jsonCategory.getString("name"));
            // category.setOrder(jsonCategory.getInt("order"));
            int nbrProducts = jsonProducts.length();
            List<Product> products = new ArrayList<>(nbrProducts);
            if (nbrProducts > 0) {
                for (int jdx = 0; jdx < nbrProducts; ++jdx) {
                    JSONObject jsonProduct = (JSONObject)jsonProducts.get(jdx);
                    Product product = new Product(jsonProduct.getString("name"));
                    // product.setChecked(jsonProduct.getBoolean("checked"));
                    // product.setOrder(jsonProduct.getInt("order"));
                    product.setCategory(category.getName());
                    products.add(product);
                }
            }
            categoriesWithProducts.add(new CategoryWithProducts().setCategory(category).setProducts(products));
        }
        return categoriesWithProducts;
    }

    @Nullable
    private JSONArray getArrayOfCategories(@NonNull String result) {
        try {
            JSONObject root = new JSONObject(result);
            return root.getJSONObject("_embedded").getJSONArray("categoryDTOList");
        } catch (JSONException e) {
            Log.e(TAG, e.toString());
            return null;
        }
    }

    @Nullable
    private String parseLists(String result) {
        try {
            final JSONArray lists = new JSONArray(result);
            for (int idx = 0; idx < lists.length(); idx++) {
                final JSONObject list = (JSONObject) lists.get(idx);
                if (list.getString("name").equals("shopping")) {
                    lastUpdate = list.getLong("lastUpdate");
                    final JSONArray links = list.getJSONArray("links");
                    for (int jdx = 0; jdx < links.length(); jdx++) {
                        final JSONObject link = (JSONObject)links.get(jdx);
                        if (link.getString("rel").equals("categories")) {
                            return link.getString("href");
                        }
                    }
                }
            }
        }
        catch (JSONException e) {
            Log.e(TAG, e.toString());
        }
        return null;
    }


    private static class MyTask extends AsyncTask<String, Void, String> {
        private final static String TAG = "RD.AsyncTask";
        private final OnTaskCompleted listener;

        MyTask(@NonNull OnTaskCompleted listener) {
            super();
            this.listener = listener;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(@NonNull String... params) {
            try {
                return get(params[0]);
            } catch (IOException e) {
                Log.e(TAG, e.toString());
                return null;
            }
        }

        @Override
        protected void onPostExecute(String result) {
            listener.onTaskCompletedSuccessfully(result);
        }

        private HttpURLConnection getConnection(@NonNull String myUrl) throws IOException {
            HttpURLConnection conn;
            URL url = new URL(myUrl);
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestProperty("User-Agent", "DFTT");
            conn.setRequestProperty("Authorization", RemoteDatabase.AUTHORIZATION);
            return conn;
        }
        @NonNull
        private String get(@NonNull String myUrl) throws IOException {
            InputStream inputStream = null;
            HttpURLConnection conn = null;

            int len = 100*1024; // only get the first n characters of the retrieve content.
            try {
                conn = getConnection(myUrl);
                // Start the query
                conn.connect();
                int response = conn.getResponseCode();
                Log.d(TAG, "The response is: " + response);
                inputStream = conn.getInputStream();

                // Convert the InputStream into a string
                return convertInputToString(inputStream, len);
            }
            catch (Exception e) {
                Log.e(TAG, e.toString());
                throw e;
            }
            finally {
                // Close the InputStream and connection
                if(conn != null) conn.disconnect();
                if (inputStream != null) inputStream.close();
            }
        }

        @NonNull
        private void post(@NonNull String myUrl, @NonNull String body) throws IOException {
            HttpURLConnection conn = null;

            try {
                conn = getConnection(myUrl);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json; utf-8");
                try(OutputStream os = conn.getOutputStream()) {
                    byte[] input = body.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }
                // Start the query
                conn.connect();
                int response = conn.getResponseCode();
                Log.d(TAG, "The response is: " + response);
            }
            catch (Exception e) {
                Log.e(TAG, e.toString());
                throw e;
            }
            finally {
                // Close the InputStream and connection
                if(conn != null) conn.disconnect();
            }
        }

        // Reads an InputStream and converts it to a String.
        private String convertInputToString(InputStream stream, int len) throws IOException {
            Reader reader = null;
            reader = new InputStreamReader(stream, "UTF-8");
            char[] buffer = new char[len];
            int n = reader.read(buffer);
            if (n == -1) Log.e(TAG, "convertInputToString failed");
            else Log.d(TAG, n + " bytes read.");
            return new String(buffer);
        }
    }


    @FunctionalInterface
    public interface LastUpdateListener {
        void onLastUpdate(long lastUpdate);
    }
}
