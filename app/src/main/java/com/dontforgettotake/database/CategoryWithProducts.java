package com.dontforgettotake.database;

import androidx.annotation.NonNull;
import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class CategoryWithProducts {
    @Embedded public Category category;
    @Relation(
            parentColumn = "name",
            entityColumn = "category_name"
    )
    public List<Product> products;

    public String getName() {
        return category.getName();
    }

    public List<Product> getProducts() {
        return products;
    }

    public Category getCategory() {
        return this.category;
    }

    public CategoryWithProducts setCategory(Category category) {
        this.category = category;
        return this;
    }

    public CategoryWithProducts setProducts(List<Product> products) {
        this.products = products;
        return this;
    }

    @NonNull
    public String toString() {
        return this.category.getName();
    }
}
