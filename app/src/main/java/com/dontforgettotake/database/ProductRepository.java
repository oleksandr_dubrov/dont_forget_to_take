package com.dontforgettotake.database;

import android.app.Application;
import androidx.lifecycle.LiveData;

import com.dontforgettotake.dialogs.CategoryDialogFragment;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Abstracted Repository as promoted by the Architecture Guide.
 * https://developer.android.com/topic/libraries/architecture/guide.html
 */
public class ProductRepository {
    private RemoteDatabase rdb;
    private ProductDao mProductsDao;
    private LiveData<List<Product>> mAllProducts;
    private LiveData<List<CategoryWithProducts>> mAllCategories;
    private List<CategoryWithProducts> mAllRemoteCategories;

    public ProductRepository(Application application, String url,
                             RemoteDatabase.LastUpdateListener listener) {
        AppDatabase db = AppDatabase.getDatabase(application);
        rdb = RemoteDatabase.getDatabase(url, listener);
        mProductsDao = db.productDao();
        mAllProducts = mProductsDao.getProducts();
        mAllCategories = mProductsDao.getCategoriesWithProducts();
        mAllRemoteCategories = null;
    }

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    public LiveData<List<Product>> getAllProducts() {
        return mAllProducts;
    }

    public LiveData<List<CategoryWithProducts>> getAllCategories() {
        return mAllCategories;
    }

    public void fetchRemote() {
        rdb.fetch();
    }
    public List<CategoryWithProducts> getAllRemoteCategories() {
        if (mAllRemoteCategories == null) {
            mAllRemoteCategories = rdb.getCategories();
        }
        return mAllRemoteCategories;
    }

    // You must call this on a non-UI thread or your app will throw an exception. Room ensures
    // that you're not doing any long running operations on the main thread, blocking the UI.
    public void insert(Product product) {
        AppDatabase.databaseWriteExecutor.execute(() -> {
            mProductsDao.insert(product);
        });
    }

    public void insert(Category category) {
        AppDatabase.databaseWriteExecutor.execute(() -> {
            mProductsDao.insert(category);
        });
    }

    public void update(Product product) {
        AppDatabase.databaseWriteExecutor.execute(() -> {
            mProductsDao.update(product);
        });
    }

    public void update(Category category) {
        AppDatabase.databaseWriteExecutor.execute(() -> {
            mProductsDao.update(category);
        });
    }

    public void delete(Product product) {
        AppDatabase.databaseWriteExecutor.execute(() -> {
            mProductsDao.delete(product);
        });
    }

    public void delete(Category category) {
        AppDatabase.databaseWriteExecutor.execute(() -> {
            mProductsDao.delete(category);
        });
    }
}
