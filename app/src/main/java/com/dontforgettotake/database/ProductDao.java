package com.dontforgettotake.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ProductDao {
    @Delete
    void delete(Product product);

    @Delete
    void delete(Category category);

    // LiveData is a data holder class that can be observed within a given lifecycle.
    // Always holds/caches latest version of data. Notifies its active observers when the
    // data has changed. Since we are getting all the contents of the database,
    // we are notified whenever any of the database contents have changed.
    @Query("SELECT * from product_table ORDER BY product_table.`order` DESC")
    LiveData<List<Product>> getProducts();

    @Query("SELECT * from categories_table ORDER BY categories_table.`order`")
    LiveData<List<Category>> getCategories();

    @Query("SELECT * FROM product_table WHERE category_name = :categoryName")
    LiveData<List<Product>> getProductsByCategory(String categoryName);

    @Transaction
    @Query("SELECT * FROM categories_table ORDER BY categories_table.`order`")
    LiveData<List<CategoryWithProducts>> getCategoriesWithProducts();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Product product);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Category category);

    @Update
    void update(Product product);

    @Update
    void update(Category category);

    @Query("DELETE FROM product_table")
    void deleteAll();
}
