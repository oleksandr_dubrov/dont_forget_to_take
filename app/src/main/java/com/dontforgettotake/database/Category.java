package com.dontforgettotake.database;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "categories_table")
public class Category {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "name")
    public String mName;

    @ColumnInfo(name = "order")
    public int mOrder;

    public Category(@NonNull String name) {
        mName = name;
        mOrder = 0;
    }

    @NonNull
    public String getName() {
        return this.mName;
    }

    @NonNull
    public String toString() {
        return mName;
    }

    public void setOrder(int order) {
        mOrder = order;
    }

    public int getOrder(){
        return mOrder;
    }
}
