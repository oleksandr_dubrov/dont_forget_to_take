package com.dontforgettotake.database;


interface OnTaskCompleted {
    void onTaskCompletedSuccessfully(String result);
}
