package com.dontforgettotake.database;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;


/**
 * Represents an item of recycler views.
 * @param <T>   a type of the item
 */
final public class RecyclerViewItem<T> {
    final private T mItem;
    final private String mName;
    private List<RecyclerViewItem> children;
    final private boolean isExtendable;
    private boolean isExtended;

    public RecyclerViewItem(@NonNull String name, @NonNull T item) {
        mName = name;
        mItem = item;
        isExtendable = false;
        children = null;
    }

    public RecyclerViewItem(@NonNull String name,
                            @NonNull T item,
                            @Nullable List<RecyclerViewItem> children,
                            boolean isExtendable) {
        this.isExtendable = isExtendable;
        mName = name;
        mItem = item;
        if (children != null)  setChildren(children);
    }

    /**
     * Set sub items.
     * @param children a sub item
     */
    private void setChildren(@NonNull List<RecyclerViewItem> children) {
        this.children = children;
    }

    /**
     * Get sub item.
     * @return a sub item
     */
    @Nullable
    public List<RecyclerViewItem> getChildren() {
        return children;
    }

    /**
     * Does it have children?
     * @return <i>true</i> if it has children, otherwise - <i>false</i>
     */
    public boolean hasChildren() {
        return children != null && children.size() > 0;
    }

    /**
     * Get a name of the item. Recycler view shows it in the list.
     * @return a name of the item
     */
    public String getName() {
        return mName;
    }

    /**
     * Get the object held in the recycler view item.
     * @return an item
     */
    public T getItem() {
        return mItem;
    }

    /**
     * Set a flag to indicate that sub items have been expose to the recycler view.
     * @param extended a flag to extended
     */
    public void setExtended(boolean extended) {
        isExtended = extended;
    }

    /**
     * Check if the recycler view item has sub items.
     * @return a flag for extendable
     */
    public boolean isExtendable() {
        return isExtendable;
    }

    /**
     * Check if sub items have been expose to the recycler view.
     * @return a flag to extended
     */
    public boolean isExtended() {
        return isExtended;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecyclerViewItem<?> that = (RecyclerViewItem<?>) o;
        return isExtendable == that.isExtendable &&
                mName.equals(that.mName);
    }

    @Override
    public int hashCode() {
        // https://stackoverflow.com/questions/113511/best-implementation-for-hashcode-method-for-a-collection
        int result = 123;
        int c = isExtendable ? 1 : 0;
        c += mName == null ? 0 : mName.hashCode();
        result = 37 * result + c;
        return result;
    }

    @Override
    public String toString() {
        return mName;
    }
}
