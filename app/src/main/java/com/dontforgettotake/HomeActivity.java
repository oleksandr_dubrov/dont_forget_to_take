package com.dontforgettotake;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.SearchView;

import com.dontforgettotake.database.Product;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;


public class HomeActivity extends AbstractActivity implements SearchView.OnQueryTextListener {
    private ProductViewModel mProductViewModel;
    private HomeAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mProductViewModel = getViewModel();

        FloatingActionButton fab = findViewById(R.id.fab_card);
        fab.setOnClickListener(view -> {
            startActivity(new Intent(this, MainActivity.class));
        });
        createRecyclerView();
    }

    private void createRecyclerView() {
        RecyclerView recyclerView = findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerView.setLayoutAnimation(animation);

        mAdapter = new HomeAdapter();
        recyclerView.setAdapter(mAdapter);

        mProductViewModel.getAllProducts().observe(this, category -> {
            mAdapter.setData(category);
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        persist();
    }

    @Override
    protected void persist() {
        List<Product> products = mAdapter.getData();
        mProductViewModel.updateProducts(products);
        super.persist();
    }

    @Override
    @NonNull
    protected String getStringFromList() {
        StringBuilder res = new StringBuilder();
        List<Product> products = mAdapter.getData();
        for (Product product : products) {
            if (product.isChecked())  res.append(product.getName()).append('\n');
        }
        return res.toString();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        searchItem.setVisible(true);
        final SearchView searchView = (SearchView)searchItem.getActionView();
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        mAdapter.filter(query);
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        mAdapter.restoreOriginData();
        return false;
    }

}
