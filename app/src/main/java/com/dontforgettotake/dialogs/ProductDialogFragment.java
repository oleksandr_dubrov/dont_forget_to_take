package com.dontforgettotake.dialogs;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AlertDialog;

import com.dontforgettotake.R;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;


final public class ProductDialogFragment extends AbstractDialogFragment
        implements AdapterView.OnItemSelectedListener {

    private List<String> categories;
    private String spinner_category_name = null;
    private ArrayAdapter<String> adapter = null;

    public ProductDialogFragment() {
        super();
        categories = Collections.emptyList();
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
        if(adapter != null) {
            adapter.addAll(categories);
            adapter.notifyDataSetChanged();
        }
    }

    @NotNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
        // Get the layout inflater
        LayoutInflater inflater = requireActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        // rationale for the suppress - https://wundermanthompsonmobile.com/2013/05/layout-inflation-as-intended/
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.fragment_product_dialog, null);
        EditText productName = view.findViewById(R.id.product);

        Spinner spinner = view.findViewById(R.id.category_spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.addAll(categories);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        InputMethodManager imgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imgr != null;
        imgr.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        DialogListener listener = getmListener();
        builder.setView(view)
                .setMessage(R.string.enter_product)
                // Add action buttons
                .setPositiveButton(R.string.add, (dialog, id) -> {
                    // Send the positive button event back to the host activity
                    String categoryName = Objects.requireNonNull(productName.getText()).toString().trim();
                    listener.onDialogPositiveClick(ProductDialogFragment.this, categoryName, spinner_category_name);
                    imgr.hideSoftInputFromWindow(productName.getWindowToken(), 0);
                })
                .setNegativeButton(R.string.cancel, (dialog, id) -> {
                    // Send the negative button event back to the host activity
                    listener.onDialogNegativeClick(ProductDialogFragment.this);
                    imgr.hideSoftInputFromWindow(productName.getWindowToken(), 0);
                });

        return builder.create();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        spinner_category_name = (String)parent.getItemAtPosition(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // nothing to do
    }
}
