package com.dontforgettotake.dialogs;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;

import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.dontforgettotake.R;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;


final public class CategoryDialogFragment extends AbstractDialogFragment {

    @NotNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
        // Get the layout inflater
        LayoutInflater inflater = requireActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        // rationale for the suppress - https://wundermanthompsonmobile.com/2013/05/layout-inflation-as-intended/
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.fragment_category_dialog, null);
        EditText categoryNameView = view.findViewById(R.id.category);
        InputMethodManager imgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imgr != null;
        imgr.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        DialogListener listener = getmListener();
        builder.setView(view)
                .setMessage(R.string.enter_category)
                // Add action buttons
                .setPositiveButton(R.string.add, (dialog, id) -> {
                    // Send the positive button event back to the host activity
                    String categoryName = Objects.requireNonNull(categoryNameView.getText()).toString().trim();
                    listener.onDialogPositiveClick(CategoryDialogFragment.this, categoryName);
                    imgr.hideSoftInputFromWindow(categoryNameView.getWindowToken(), 0);
                })
                .setNegativeButton(R.string.cancel, (dialog, id) -> {
                    // Send the negative button event back to the host activity
                    listener.onDialogNegativeClick(CategoryDialogFragment.this);
                    imgr.hideSoftInputFromWindow(categoryNameView.getWindowToken(), 0);
                });

        return builder.create();
    }
}
