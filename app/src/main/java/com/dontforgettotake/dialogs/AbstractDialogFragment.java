package com.dontforgettotake.dialogs;


import android.content.Context;

import androidx.fragment.app.DialogFragment;

import org.jetbrains.annotations.NotNull;

abstract public class AbstractDialogFragment extends DialogFragment {
    // Use this instance of the interface to deliver action events
    private DialogListener mListener;

    public AbstractDialogFragment() {
        // Required empty public constructor
    }

    /* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */
    public interface DialogListener {
        void onDialogPositiveClick(DialogFragment dialog, String... data);
        void onDialogNegativeClick(DialogFragment dialog);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (DialogListener) context;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString() + " must implement DialogListener");
        }
    }

    DialogListener getmListener() {
        return mListener;
    }
}
