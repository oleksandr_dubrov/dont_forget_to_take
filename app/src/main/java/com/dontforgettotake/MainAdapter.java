package com.dontforgettotake;

import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dontforgettotake.database.Category;
import com.dontforgettotake.database.CategoryWithProducts;
import com.dontforgettotake.database.Product;
import com.dontforgettotake.database.RecyclerViewItem;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;


final public class MainAdapter extends RecyclerView.Adapter<MainAdapter.MyViewHolder> {
    private static final String TAG = "MainAdapter";
    private List<RecyclerViewItem> mDataset = new ArrayList<>();
    private MainActivity context;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView textView;
        ImageView imageView;

        MyViewHolder(View v) {
            super(v);
            textView = v.findViewById(R.id.tv_item_img);
            imageView = v.findViewById(R.id.iconView);

            v.setOnLongClickListener(v1 -> {
                collapseAll();
                return true;
            });

            v.setOnClickListener(v1 -> {
                int posIdx = getAdapterPosition();
                RecyclerViewItem<?> cur = mDataset.get(posIdx);
                if(cur.isExtendable()) {
                    List<RecyclerViewItem> ch = cur.getChildren();
                    if (!cur.hasChildren()) {
                        Toast toast = Toast.makeText(context, "The group is empty.", Toast.LENGTH_SHORT);
                        toast.show();
                    }
                    else {
                        assert ch != null;
                        if (cur.isExtended()) {
                            mDataset.removeAll(ch);
                            notifyItemRangeRemoved(posIdx + 1, ch.size());
                        } else {
                            mDataset.addAll(posIdx + 1, ch);
                            notifyItemRangeInserted(posIdx + 1, ch.size());
                        }
                        cur.setExtended(!cur.isExtended());
                        setItemIcon(imageView, cur);
                    }
                }
                else {
                    Product product = (Product)cur.getItem();
                    product.setChecked(!product.isChecked());
                    textView.setPaintFlags(product.isChecked() ? textView.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG)
                            : textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                }
            });
        }
    }

    MainAdapter(MainActivity parent) {
        context = parent;
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public MainAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                       int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_item_img_text, parent, false);
        return new MyViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        RecyclerViewItem rvi = mDataset.get(position);
        setItemIcon(holder.imageView, rvi);
        holder.textView.setText(rvi.getName());
        if (rvi.getItem() instanceof Product) {
            boolean c = ((Product) rvi.getItem()).isChecked();
            holder.textView.setPaintFlags( c ? holder.textView.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG)
                    : holder.textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
        else {
            holder.textView.setPaintFlags(holder.textView.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
        }
    }

    /**
     * Set icon depended on a given RecyclerViewItem or make it invisible.
     * @param imageView the view where an icon to be set
     * @param rvi recycle view item
     */
    private void setItemIcon(@NonNull ImageView imageView, @NonNull RecyclerViewItem rvi) {
        if (rvi.isExtendable()) {
            if (!rvi.hasChildren()) {
                imageView.setImageResource(android.R.drawable.button_onoff_indicator_on);
            }
            int id = rvi.isExtended() ? android.R.drawable.button_onoff_indicator_on : android.R.drawable.ic_input_add;
            imageView.setImageResource(id);
            imageView.setVisibility(View.VISIBLE);
        }
        else {
            imageView.setVisibility(View.INVISIBLE);
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    /**
     * Convert data from DB representation to items suitable for the recycler list
     * @param categories   data from DB.
     */
    void setData(@NonNull List<CategoryWithProducts> categories) {
        List<RecyclerViewItem> newData = new ArrayList<>(categories.size());
        for (CategoryWithProducts d : categories) {
            List<RecyclerViewItem> children = new ArrayList<>(d.products.size());
            for (Product product: d.products) {
                // add only checked products. Maybe it could be done via SQL request,
                // but it seems to complex for now.
                if (product.isChecked()) {
                    children.add(new RecyclerViewItem<>(product.getName(), product));
                }
            }
            if (!children.isEmpty()) {
                RecyclerViewItem<?> rvi = new RecyclerViewItem<>(d.getCategory().getName(),
                        d.getCategory(),
                        children, true);

                // keep collapsed/expended state
                int idx = mDataset.indexOf(rvi);
                if (idx != -1 && mDataset.get(idx).isExtended()) {
                    newData.addAll(extendRvi(rvi));
                }
                else {
                    newData.add(rvi);
                }
            }
        }
        mDataset = newData;
        notifyDataSetChanged();
    }

    /**
     * Return data in a form it has been set in <code>setData</code>.
     * @return a list of categories with products
     */
    List<CategoryWithProducts> getData() {
        List<CategoryWithProducts> categoryWithProducts = new ArrayList<>(getSizeOnScreen());
        for (RecyclerViewItem rvi : mDataset) {
            if (rvi.isExtendable()) {
                CategoryWithProducts dp = new CategoryWithProducts();
                List<Product> pr = null;
                if (rvi.getChildren() != null) {
                    pr = new ArrayList<>(rvi.getChildren().size());
                    List l = rvi.getChildren();
                    for (Object p : l) {
                        pr.add((Product) ((RecyclerViewItem) p).getItem());
                    }
                }
                dp.setCategory((Category) rvi.getItem()).setProducts(pr);
                categoryWithProducts.add(dp);
            }
        }
        return categoryWithProducts;
    }

    /**
     * Calculate the size of the data, that is sent to the recycle view.
     * @return a size of the list
     */
    private int getSizeOnScreen() {
        int size = mDataset.size();
        for (RecyclerViewItem rvi : mDataset) {
            if (rvi.getChildren() != null) {
                size += rvi.getChildren().size();
            }
        }
        return size;
    }

    /**
     * Allow to drag categories only.
     * @param pos the adapter position returned by <code>getAdapterPosition()</code>
     * @return <tt>true</tt> if the holder can be moved e.g.
     */
    boolean canBeDragged(int pos) {
        RecyclerViewItem cur = mDataset.get(pos);
        return cur.isExtendable();
    }

    /**
     * Collapse all.
     */
    void collapseAll(){
        expandOrCollapse(false);
    }

    /**
     * Expand all
     */
    void expendAll() {
        expandOrCollapse(true);
    }

    /**
     * Extend the item to list with its children.
     * @param rvi an recycler view item
     * @return a list of the item and its children
     */
    private List<RecyclerViewItem> extendRvi (@NonNull RecyclerViewItem<?> rvi) {
        if (!rvi.isExtendable()) {
            Log.e(TAG, rvi.getName() + " is not extendable.");
            if (BuildConfig.DEBUG) throw new AssertionError("cannot extend");
        }

        List<RecyclerViewItem> exRvi = null;
        if (rvi.isExtended()) {
            Log.w(TAG, rvi.getName() + " is already extended.");
        }
        else if (rvi.hasChildren()) {
            exRvi = new ArrayList<>(Objects.requireNonNull(rvi.getChildren()).size() + 1);
            rvi.setExtended(true);
            exRvi.add(rvi);
            exRvi.addAll(rvi.getChildren());
            return exRvi;
        }
        else {
            exRvi = Collections.singletonList(rvi);
        }
        return exRvi;
    }

    private void expandOrCollapse(boolean expand) {
        List<RecyclerViewItem> newDataSet = new ArrayList<>(mDataset.size());
        for (RecyclerViewItem<?> item : mDataset) {
            if (item.isExtendable()) {
                item.setExtended(expand);
                newDataSet.add(item);
                List<RecyclerViewItem>  ch = item.getChildren();
                if (expand && ch != null) {
                    newDataSet.addAll(ch);
                }
            }
        }
        mDataset = newDataSet;
        notifyDataSetChanged();
    }

    /**
     * Uncheck an item and all its children if any
     * @param i position
     */
    void uncheckItem(int i) {
        RecyclerViewItem rvi = mDataset.get(i);
        if (rvi.isExtendable() && rvi.hasChildren()) {
            assert rvi.getChildren() != null;
            int nChildren = rvi.getChildren().size();
            for (int idx = 0; idx < nChildren; idx++) {
                RecyclerViewItem r = (RecyclerViewItem)rvi.getChildren().get(idx);
                Product p = (Product) r.getItem();
                p.setChecked(false);
            }
        }
        else {
            Product p = (Product)mDataset.get(i).getItem();
            p.setChecked(false);
        }
    }

    @Nullable Product getProductById(int i) {
        RecyclerViewItem rvi = mDataset.get(i);
        if (rvi.getItem() instanceof Product) {
            return (Product)rvi.getItem();
        }
        return null;
    }

    @Nullable CategoryWithProducts getCategory(int i) {
        RecyclerViewItem<?> rvi = mDataset.get(i);
        List<Product> products = null;
        if (rvi.getItem() instanceof Category) {
            Category c = (Category)rvi.getItem();
            CategoryWithProducts category = new CategoryWithProducts().setCategory(c);
            if (rvi.getChildren() != null) {
                List<RecyclerViewItem> prods = rvi.getChildren();
                products = new ArrayList<>(prods.size());
                for (RecyclerViewItem r : prods) {
                    products.add((Product)r.getItem());
                }
            }
            category.setProducts(products);

            return category;
        }
        return null;
    }
}
