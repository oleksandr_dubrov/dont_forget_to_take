package com.dontforgettotake.modify;

import android.os.Bundle;
import android.view.Menu;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dontforgettotake.AbstractActivity;
import com.dontforgettotake.ProductViewModel;
import com.dontforgettotake.R;
import com.dontforgettotake.dialogs.AbstractDialogFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import static androidx.recyclerview.widget.ItemTouchHelper.DOWN;
import static androidx.recyclerview.widget.ItemTouchHelper.RIGHT;
import static androidx.recyclerview.widget.ItemTouchHelper.UP;


abstract public class AbstractModifyActivity extends AbstractActivity
        implements AbstractDialogFragment.DialogListener{
    private ProductViewModel mProductViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify);
        mProductViewModel = new ViewModelProvider(this).get(ProductViewModel.class);
        FloatingActionButton fab = findViewById(R.id.fab_plus);
        fab.setOnClickListener(view -> {
            showAddDialog();
        });
    }

    protected ProductViewModel getProductViewModel() {
        return mProductViewModel;
    }

    protected RecyclerView createRecyclerView() {
        RecyclerView recyclerView = findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerView.setLayoutAnimation(animation);

        ModifyAdapter<?> adapter = getAdapter();

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(UP|DOWN, RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView,
                                  @NonNull RecyclerView.ViewHolder viewHolder,
                                  @NonNull RecyclerView.ViewHolder target) {
                adapter.swap(viewHolder.getAdapterPosition(), target.getAdapterPosition());
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                if (i == RIGHT) {
                    int pos = viewHolder.getAdapterPosition();
                    showRemoveDialog(pos);
                }
            }
        }).attachToRecyclerView(recyclerView);
        return recyclerView;
    }

    public boolean onPrepareOptionsMenu(@NonNull Menu menu) {
        menu.findItem(R.id.action_share).setVisible(false);
        return true;
    }

    @Override
    protected String getStringFromList() {
        return null;
    }

    /**
     * Get Adapter.
     * @return ModifyAdapter
     */
    abstract protected ModifyAdapter getAdapter();

    /**
     * Create an instance of the adding dialog fragment and show it.
     */
    abstract public void showAddDialog();

    /**
     * Create an instance of the removing dialog fragment  and show it
     * @param pos position of an item that's about to remove
     */
    abstract void showRemoveDialog(int pos);
}
