package com.dontforgettotake.modify;

import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.LiveData;

import com.dontforgettotake.BuildConfig;
import com.dontforgettotake.database.CategoryWithProducts;
import com.dontforgettotake.database.Product;
import com.dontforgettotake.dialogs.ProductDialogFragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


final public class ModifyProductActivity extends AbstractModifyActivity {
    private static final String TAG = "MainAdapter";
    private ModifyAdapter<Product> mAdapter = new ModifyAdapter<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getProductViewModel().getAllProducts().observe(this, products -> {
            mAdapter.setData(products);
        });
        createRecyclerView().setAdapter(mAdapter);
    }

    @Override
    protected ModifyAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void showAddDialog() {
        ProductDialogFragment dialog = new ProductDialogFragment();
        final LiveData<List<CategoryWithProducts>> allCategories = getProductViewModel().getAllCategories();
        allCategories.observe(this, categories -> {
            List<String> names = new ArrayList<>(categories.size());
            for (CategoryWithProducts category : categories) {
                names.add(category.getName());
            }
            dialog.setCategories(names);
        });
        dialog.show(getSupportFragmentManager(), "ProductDialogFragment");
    }

    @Override
    public void showRemoveDialog(int pos) {
        Product category = mAdapter.getItemByPosIdx(pos);
        getProductViewModel().delete(category);
        mAdapter.removeProduct(pos);
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog, @NonNull String... names) {
        if (names.length != 2) {
            String msg = "exactly 2 names required";
            Log.e(TAG, msg);
            if (BuildConfig.DEBUG) throw new AssertionError(msg);
        }
        Product product = new Product(names[0]);
        product.setCategory(names[1]);
        product.setChecked(true);
        getProductViewModel().insertProduct(product);
        Log.i(TAG, names[1] +  ":" + names[0] + " added.");
        mAdapter.extendData(Collections.singletonList(product));
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        // nothing to do
    }
}
