package com.dontforgettotake.modify;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.dontforgettotake.R;
import com.dontforgettotake.database.Category;
import com.dontforgettotake.database.CategoryWithProducts;
import com.dontforgettotake.database.Product;
import com.dontforgettotake.dialogs.CategoryDialogFragment;

import java.util.Collections;
import java.util.List;


final public class ModifyCategoryActivity extends AbstractModifyActivity implements CategoryDialogFragment.DialogListener {

    private ModifyAdapter<CategoryWithProducts> mAdapter = new ModifyAdapter<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getProductViewModel().getAllCategories().observe(this, categories -> {
            mAdapter.setData(categories);
        });
        createRecyclerView().setAdapter(mAdapter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        persist();
    }

    @Override
    protected void persist() {
        List<CategoryWithProducts> categories = mAdapter.getData();
        updateOrderNumber(categories);
        getProductViewModel().updateCategoriesWithProducts(categories);
        super.persist();
    }

    @Override
    protected ModifyAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void showAddDialog() {
        DialogFragment dialog = new CategoryDialogFragment();
        dialog.show(getSupportFragmentManager(), "CategoryDialogFragment");
    }

    @Override
    public void showRemoveDialog(int pos) {
        CategoryWithProducts categoryWithProducts = mAdapter.getItemByPosIdx(pos);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(String.format( getString(R.string.remove_category_message),
                                         categoryWithProducts.getName()))
               .setTitle(R.string.remove_category_title);
        builder.setPositiveButton(R.string.ok, (dialog, id) -> {
            for (Product product : categoryWithProducts.getProducts()) {
                getProductViewModel().delete(product);
            }
            getProductViewModel().delete(categoryWithProducts.getCategory());
        });
        builder.setNegativeButton(R.string.cancel, (dialog, id) -> { });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    // The dialog fragment receives a reference to this Activity through the
    // Fragment.onAttach() callback, which it uses to call the following methods
    // defined by the interface
    @Override
    public void onDialogPositiveClick(DialogFragment dialog, String... categoryName) {
        Category category = new Category(categoryName[0]);
        getProductViewModel().insertCategory(category);
        CategoryWithProducts categoryWithProducts = new CategoryWithProducts().setCategory(category);
        mAdapter.extendData(Collections.singletonList(categoryWithProducts));
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        // User touched the dialog's negative button
    }

    private void updateOrderNumber(@NonNull List<CategoryWithProducts> categories) {
        for (int idx = 0; idx < categories.size(); ++idx) {
            categories.get(idx).getCategory().setOrder(idx);
        }
    }
}
