package com.dontforgettotake.modify;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dontforgettotake.R;
import com.dontforgettotake.database.CategoryWithProducts;
import com.dontforgettotake.database.Product;
import com.dontforgettotake.database.RecyclerViewItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


final public class ModifyAdapter<T> extends RecyclerView.Adapter<ModifyAdapter.MyViewHolder> {

    private List<RecyclerViewItem<T>> mDataSet = new ArrayList<>();

    @NonNull
    @Override
    public ModifyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_item_img_text, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ModifyAdapter.MyViewHolder holder, int position) {
        T item = mDataSet.get(position).getItem();
        if (item instanceof Product) {
            holder.textView.setText(((Product)item).getName());
            return;
        }
        if (item instanceof CategoryWithProducts) {
            holder.textView.setText(((CategoryWithProducts)item).getCategory().getName());
        }
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    /**
     * Swap holders in the data set.
     * This method came from Stackoverflow.
     * @param fromPos from this position
     * @param toPos to this position
     */
    void swap(int fromPos, int toPos) {
        if (fromPos < toPos) {
            for (int i = fromPos; i < toPos; i++)
                Collections.swap(mDataSet, i, i + 1);
        }
        else {
            for (int i = fromPos; i > toPos; i--)
                Collections.swap(mDataSet, i, i - 1);
        }
        notifyItemMoved(fromPos, toPos);
    }

    @NonNull
    private static <T> List<RecyclerViewItem<T>> convertToRVIs(@NonNull List<T> data) {
        List<RecyclerViewItem<T>> rvi = new ArrayList<>(data.size());
        for (T t : data) {
            rvi.add(new RecyclerViewItem<>(t.toString(), t));
        }
        return rvi;
    }

    void setData(List<T> data) {
        mDataSet = convertToRVIs(data);
        notifyDataSetChanged();
    }

    void extendData(List<T> data) {
        mDataSet.addAll(convertToRVIs(data));
        notifyDataSetChanged();
    }

    T getItemByPosIdx(int adapterPosition) {
        return mDataSet.get(adapterPosition).getItem();
    }

    void removeProduct(int adapterPosition) {
        mDataSet.remove(adapterPosition);
        notifyItemRemoved(adapterPosition);
    }

    List<T> getData() {
        List<T> data = new ArrayList<>(mDataSet.size());
        for (RecyclerViewItem<T> rvi : mDataSet){
            data.add(rvi.getItem());
        }
        return data;
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.tv_item_img);
            imageView = itemView.findViewById(R.id.iconView);
            imageView.setImageResource(android.R.drawable.sym_contact_card);
        }
    }
}
